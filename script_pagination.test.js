var jsdom = require("jsdom");
var JSDOM = jsdom.JSDOM;
global.document = new JSDOM(html).window.document;
const subscribe = require('./script_pagination');

test('change button text after a click', () => {
  document.body.innerHTML = "<div class='feed' id='1'><h3>Feeds Testing</h3><button type='button' onclick='subscribe(this)'>subscribe</button><p>Subscribed by: <span class='likes'>0</span></p></div>";

  $('button').click();
  expect(subscribe).toBeCalled;
  expect($('button').innerHTML).toEqual('unsubscribe');

});

test('increase likes count after a click', () => {
  document.body.innerHTML = "<div class='feed' id='1'><h3>Feeds Testing</h3><button type='button' onclick='subscribe(this)'>subscribe</button><p>Subscribed by: <span class='likes'>0</span></p></div>";

  $('button').click();
  expect(subscribe).toBeCalled;
  expect($('.likes').innerHTML).toEqual('1');

});
