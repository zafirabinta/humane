# humane

functionalities implemented: 
- [x] Have a dashboard or main page
- [x] On the main page have a list of feeds
- [x] Eachfeed will have a subscribe/unsubscribe button 
- [x] Once subscribed we can only unsubscribe from the feed and then subscribe again
- [x] Display the subscriptionId once the feed is subscribed under the feed
- [x] At the top please show the total number of subscribed feeds
- [x] Add pagination for the list of feeds
- [x] Add a filter by feedName to filter out the rest of the feeds
- [x] By default the feeds should be sorted by feedId
- [x] All the state lasts for only the user session, do not bother persisting it

assumptions:
- subscriptionId is the number of subscriber on that feed

how to run and use the app:
1. download and place all files in the same folder
![all files in the same folder](/images/1.jpg)
2. double click on **homepage.html** file
3. your browser will open the html page soon
![homepage](/images/2.jpg)
4. you can subscribe feed by clicking on the subscribe button
5. The subscribe button will change to unsubscribe button. The number of **subscribed by** and **subscribed feeds** will increase
![subsribe feed](/images/3.jpg)
6. you can go to the next page by clicking **next** on the bottom of the page. The app will show the second page
![second page](/images/4.jpg)
7. you can also subscribe the feeds on the other page
8. you can see the number of **subscribed by** and **subscribed feeds** are increasing
![subscribe feeds](/images/5.jpg)
9. after subscribing, you can unsubscribe the feeds
10. you can see the number of **subscribed by** and **subscribed feeds** are decreasing and the unsubscribe button will change to subscribe button
![unsubscribe feed](/images/6.jpg)
11. you can search the feed by feed's name using the search bar at the top of the page
12. the page will show all the mathcing feeds
![search feeds](/images/8.jpg)
![search feed](/images/7.jpg)
13. if there is no mathcing feed, the page will show nothing
![no feed](/images/9.jpg)

unit test:

I have created some unit tests for the app. But I can't run the unit tests on the single Javascript file. When I run the test (using `yarn test`), it will show errors on the 'document' and 'window' object on my Javascript file. I couldn't find any reference to solve this problem.:disappointed:

references:

- https://stackoverflow.com/questions/25434813/simple-pagination-in-javascript
- https://www.w3schools.com/howto/howto_js_filter_lists.asp
- https://jestjs.io/docs/tutorial-jquery
